﻿using DevExpress.ExpressApp.Utils;

namespace EasyXaf.ExcelImporters;

public static class TypeExtensions
{
    public static bool IsNumericType(this Type type)
    {
        if (type.IsPrimitive)
        {
            if (type == typeof(bool) || type == typeof(IntPtr) || type == typeof(UIntPtr))
            {
                return false;
            }
            return true;
        }
        return false;
    }

    public static bool IsWholeNumberType(this Type type)
    {
        var wholeNumberTypes = new Type[]
        {
            typeof(long),
            typeof(ulong),
            typeof(int),
            typeof(uint),
            typeof(short),
            typeof(ushort),
            typeof(byte),
            typeof(sbyte),
        };
        return wholeNumberTypes.Contains(type);
    }

    public static IEnumerable<string> GetEnumValueTexts(this Type enumType)
    {
        if (enumType.IsEnum)
        {
            var enumDescriptor = new EnumDescriptor(EnumDescriptor.GetEnumType(enumType));
            foreach (var value in enumDescriptor.Values)
            {
                yield return enumDescriptor.GetCaption(value);
            }
        }
    }
}
