﻿using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Utils;
using System.ComponentModel;

namespace EasyXaf.ExcelImporters;

[DisplayProperty(nameof(DisplayName))]
public interface IModelImportingValidation : IModelNode
{
    [Browsable(false)]
    string DisplayName { get; }

    string Name { get; set; }

    [CriteriaOptions("TargetType")]
    [Editor(CriteriaEditorHelper.TypeName, ControlConstants.UITypeEditor)]
    string Criteria { get; set; }

    [DefaultValue(false)]
    bool InvertResult { get; set; }

    string UsedProperties { get; set; }

    string MessageTemplate { get; set; }

    [Browsable(false)]
    Type TargetType { get; }

    [Browsable(false)]
    [ModelValueCalculator("(IModelImporting)Parent.Parent")]
    IModelImporting Importing { get; }
}
