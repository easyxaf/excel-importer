﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;

namespace EasyXaf.ExcelImporters;

public class ExcelImportersModule : ModuleBase
{
    public override void ExtendModelInterfaces(ModelInterfaceExtenders extenders)
    {
        base.ExtendModelInterfaces(extenders);
        extenders.Add<IModelApplication, IModelApplicationImportings>();
    }
}