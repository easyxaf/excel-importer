﻿namespace EasyXaf.ExcelImporters;

public static class NumericHelper
{
    public static T? GetNumericValue<T>(string valueText) where T : struct
    {
        if (typeof(T) == typeof(sbyte))
        {
            if (sbyte.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        else if (typeof(T) == typeof(byte))
        {
            if (byte.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        else if (typeof(T) == typeof(short))
        {
            if (short.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        else if (typeof(T) == typeof(ushort))
        {
            if (ushort.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        else if (typeof(T) == typeof(int))
        {
            if (int.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        else if (typeof(T) == typeof(uint))
        {
            if (uint.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        else if (typeof(T) == typeof(long))
        {
            if (long.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        else if (typeof(T) == typeof(ulong))
        {
            if (ulong.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        else if (typeof(T) == typeof(float))
        {
            if (float.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        else if (typeof(T) == typeof(double))
        {
            if (double.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        else if (typeof(T) == typeof(decimal))
        {
            if (decimal.TryParse(valueText, out var typedValue))
            {
                return (T)Convert.ChangeType(typedValue, typeof(T));
            }
        }
        return default;
    }

    public static object GetNumericValue(string valueText, Type dataType)
    {
        if (dataType == typeof(sbyte))
        {
            return GetNumericValue<sbyte>(valueText);
        }
        else if (dataType == typeof(byte))
        {
            return GetNumericValue<byte>(valueText);
        }
        else if (dataType == typeof(short))
        {
            return GetNumericValue<short>(valueText);
        }
        else if (dataType == typeof(ushort))
        {
            return GetNumericValue<ushort>(valueText);
        }
        else if (dataType == typeof(int))
        {
            return GetNumericValue<int>(valueText);
        }
        else if (dataType == typeof(uint))
        {
            return GetNumericValue<uint>(valueText);
        }
        else if (dataType == typeof(long))
        {
            return GetNumericValue<long>(valueText);
        }
        else if (dataType == typeof(ulong))
        {
            return GetNumericValue<ulong>(valueText);
        }
        else if (dataType == typeof(float))
        {
            return GetNumericValue<float>(valueText);
        }
        else if (dataType == typeof(double))
        {
            return GetNumericValue<double>(valueText);
        }
        else if (dataType == typeof(decimal))
        {
            return GetNumericValue<decimal>(valueText);
        }
        return default;
    }
}
