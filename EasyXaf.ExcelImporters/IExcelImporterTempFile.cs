﻿namespace EasyXaf.ExcelImporters;

public interface IExcelImporterTempFile
{
    bool IsEmpty { get; }

    string FileName { get; set; }

    byte[] FileContent { get; set; }

    void InitFile(string fileName, byte[] fileContent);

    void SaveToStream(Stream stream);
}
