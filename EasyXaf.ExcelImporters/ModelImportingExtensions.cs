﻿namespace EasyXaf.ExcelImporters;

public static class ModelImportingExtensions
{
    public static string GetCaption(this IModelImporting importingModel)
    {
        var caption = importingModel.Caption;
        if (string.IsNullOrWhiteSpace(caption))
        {
            caption = importingModel.ModelClass.Caption;
        }
        if (string.IsNullOrWhiteSpace(caption))
        {
            caption = importingModel.ModelClass.Name;
        }
        return caption;
    }
}
