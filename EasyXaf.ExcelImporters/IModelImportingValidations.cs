﻿using DevExpress.ExpressApp.Model;

namespace EasyXaf.ExcelImporters;

public interface IModelImportingValidations : IModelNode, IModelList<IModelImportingValidation>
{
}
