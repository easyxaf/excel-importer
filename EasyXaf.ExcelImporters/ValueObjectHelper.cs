﻿using DevExpress.Spreadsheet;
using System.Reflection;

namespace EasyXaf.ExcelImporters;

public static class ValueObjectHelper
{
    public static ValueObject FromValue(string value)
    {
        var valueObjectType = typeof(ValueObject);
        var constructor = valueObjectType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(string), typeof(string) }, null);
        return (ValueObject)constructor.Invoke(new object[] { string.Empty, value });
    }
}
