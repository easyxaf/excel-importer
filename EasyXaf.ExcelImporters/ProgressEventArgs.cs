﻿namespace EasyXaf.ExcelImporters;

public class ProgressEventArgs : EventArgs
{
    public int TotalCount { get; }

    public int CompletedCount { get; }

    public int Percentage => (int)Math.Floor((double)CompletedCount / TotalCount) * 100;

    public ProgressEventArgs(int totalCount, int completedCount)
    {
        TotalCount = totalCount;
        CompletedCount = completedCount;
    }
}
