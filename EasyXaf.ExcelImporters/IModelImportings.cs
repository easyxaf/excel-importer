﻿using DevExpress.ExpressApp.Model;

namespace EasyXaf.ExcelImporters;

public interface IModelImportings : IModelNode, IModelList<IModelImporting>
{
}
