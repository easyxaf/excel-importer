﻿using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Utils;
using System.ComponentModel;
using System.Drawing;

namespace EasyXaf.ExcelImporters;

[DisplayProperty(nameof(DisplayName))]
public interface IModelImportingMember : IModelNode
{
    [Browsable(false)]
    string DisplayName { get; }

    string Caption { get; set; }

    [CriteriaOptions("Property.Type")]
    [Editor(CriteriaEditorHelper.TypeName, ControlConstants.UITypeEditor)]
    [Description("值类型可以通过Criteria转换值，引用类型可以通过Criteria获取值")]
    string ValueCriteria { get; set; }

    [CriteriaOptions("Property.Type")]
    [Editor(CriteriaEditorHelper.TypeName, ControlConstants.UITypeEditor)]
    [Description("值类型可以通过Criteria转换值，引用类型可以通过Criteria获取值")]
    string DefaultValueCriteria { get; set; }

    string MinimumValue { get; set; }

    string MaximumValue { get; set; }

    [Category("Column")]
    [DefaultValue(null)]
    Color? HeaderFontColor { get; set; }

    [Category("Column")]
    [DefaultValue(null)]
    Color? HeaderBackgroundColor { get; set; }

    [Category("Column")]
    int? ColumnWidth { get; set; }

    [Category("Column")]
    int? ColumnIndex { get; set; }

    [Category("Column")]
    string NumberFormat { get; set; }

    [Category("Column")]
    [DefaultValue(false)]
    bool IgnoreValidationError { get; set; }

    [Category("Column")]
    [DefaultValue(true)]
    [Description("设置在Excel模板中是否显示列，不显示时它只将Excel中的列隐藏，但列还是保留的")]
    bool ColumnVisible { get; set; }

    [Category("Column")]
    [DefaultValue(false)]
    [Description("导入Excel数据时，必须包含当前列，如果列不是必填列，默认可以不包含")]
    bool MustIncludeColumn { get; set; }

    [DefaultValue(true)]
    [Description("设置是否显示在Excel模板中，不显示时Excel模板中也不包含它")]
    bool VisibleInTemplate { get; set; }

    [DefaultValue(true)]
    [Description("当导入时是否允许使用Excel模板中的列更新")]
    bool AllowUpdateMember { get; set; }

    [Description("预定义值可以为列提供下拉选择，它的优先级最高")]
    string PredefinedValues { get; set; }

    [CriteriaOptions("Property.Type")]
    [Editor(CriteriaEditorHelper.TypeName, ControlConstants.UITypeEditor)]
    string DataSourceCriteria { get; set; }

    [Browsable(false)]
    ICollection<IModelMember> Members { get; }

    [Required]
    [Category("Data")]
    [DataSourceProperty("Members")]
    [ModelPersistentName("PropertyName")]
    IModelMember Property { get; set; }

    [Browsable(false)]
    [ModelValueCalculator("(IModelImporting)Parent.Parent")]
    IModelImporting Importing { get; }
}
