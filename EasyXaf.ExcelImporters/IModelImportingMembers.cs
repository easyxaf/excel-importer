﻿using DevExpress.ExpressApp.Model;

namespace EasyXaf.ExcelImporters;

public interface IModelImportingMembers : IModelNode, IModelList<IModelImportingMember>
{
}
