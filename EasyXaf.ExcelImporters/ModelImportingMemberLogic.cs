﻿using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.Core;

namespace EasyXaf.ExcelImporters;

[DomainLogic(typeof(IModelImportingMember))]
public static class ModelImportingMemberLogic
{
    public static ICollection<IModelMember> Get_Members(IModelImportingMember importingMemberModel)
    {
        var modelClass = importingMemberModel.Importing.ModelClass;
        if (modelClass != null)
        {
            return modelClass.AllMembers;
        }
        return Array.Empty<IModelMember>();
    }

    public static string Get_DisplayName(IModelImportingMember importingMemberModel)
    {
        if (!string.IsNullOrWhiteSpace(importingMemberModel.Caption))
        {
            return importingMemberModel.Caption;
        }
        else if (importingMemberModel.Property != null)
        {
            if (!string.IsNullOrWhiteSpace(importingMemberModel.Property.Caption))
            {
                return importingMemberModel.Property.Caption;
            }
            else
            {
                return importingMemberModel.Property.Name;
            }
        }
        return ((ModelNode)importingMemberModel).Id;
    }
}
