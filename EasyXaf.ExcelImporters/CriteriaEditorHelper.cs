﻿using DevExpress.ExpressApp;

namespace EasyXaf.ExcelImporters;

public static class CriteriaEditorHelper
{
    public const string TypeName = "DevExpress.ExpressApp.Win.Core.ModelEditor.CriteriaModelEditorControl, DevExpress.ExpressApp.Win" + XafAssemblyInfo.VersionSuffix + XafAssemblyInfo.AssemblyNamePostfix;
}
