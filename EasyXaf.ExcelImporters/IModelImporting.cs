﻿using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Utils;
using System.ComponentModel;
using System.Drawing;

namespace EasyXaf.ExcelImporters;

[DisplayProperty("DisplayName")]
public interface IModelImporting : IModelNode
{
    [Browsable(false)]
    string DisplayName { get; }

    string Caption { get; set; }

    int StartRow { get; set; }

    int StartColumn { get; set; }

    [DefaultValue(true)]
    bool Enable { get; set; }

    [DefaultValue(false)]
    [Description("在导出Excel模板时，是否包含当前视图的数据")]
    bool IsExportViewData { get; set; }

    [DefaultValue(ImportingMode.Synchronize)]
    [Description("导入数据的方式，默认为同步，不存在的则创建，存在的则更新")]
    ImportingMode ImportingMode { get; set; }

    [Category("Column")]
    [DefaultValue(null)]
    Color? HeaderFontColor { get; set; }

    [Category("Column")]
    [DefaultValue(null)]
    Color? HeaderBackgroundColor { get; set; }

    [Category("Column")]
    [DefaultValue(null)]
    Color? ErrorBackgroundColor { get; set; }

    [DataSourceProperty("Members")]
    IModelImportingMember KeyMember { get; set; }

    [CriteriaOptions("ModelClassType")]
    [Editor(CriteriaEditorHelper.TypeName, ControlConstants.UITypeEditor)]
    string KeyCriteria { get; set; }

    [Browsable(false)]
    Type ModelClassType { get; }

    [Required]
    [Category("Data")]
    [DataSourceProperty("Application.BOModel")]
    IModelClass ModelClass { get; set; }

    IModelImportingMembers Members { get; }

    IModelImportingValidations Validations { get; }
}
