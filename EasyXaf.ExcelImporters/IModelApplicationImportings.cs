﻿namespace EasyXaf.ExcelImporters;

public interface IModelApplicationImportings
{
    IModelImportings Importings { get; }
}
