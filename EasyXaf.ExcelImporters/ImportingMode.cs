﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.ExcelImporters;

public enum ImportingMode
{
    [XafDisplayName("同步")]
    Synchronize,

    [XafDisplayName("仅创建")]
    OnlyCreation,

    [XafDisplayName("仅更新")]
    OnlyUpdate,

    [XafDisplayName("全创建")]
    AllCreation
}
