﻿using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model.Core;

namespace EasyXaf.ExcelImporters;

[DomainLogic(typeof(IModelImportingValidation))]
public static class ModelImportingValidationLogic
{
    public static string Get_DisplayName(IModelImportingValidation importingValidationModel)
    {
        if (!string.IsNullOrWhiteSpace(importingValidationModel.Name))
        {
            return importingValidationModel.Name;
        }
        return ((ModelNode)importingValidationModel).Id;
    }

    public static Type Get_TargetType(IModelImportingValidation importingValidationModel)
    {
        return importingValidationModel.Importing.ModelClassType;
    }
}
