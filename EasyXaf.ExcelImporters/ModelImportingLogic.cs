﻿using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model.Core;

namespace EasyXaf.ExcelImporters;

[DomainLogic(typeof(IModelImporting))]
public static class ModelImportingLogic
{
    public static string Get_DisplayName(IModelImporting importingModel)
    {
        if (!string.IsNullOrWhiteSpace(importingModel.Caption))
        {
            return importingModel.Caption;
        }
        else if (importingModel.ModelClass != null)
        {
            if (!string.IsNullOrEmpty(importingModel.ModelClass.Caption))
            {
                return importingModel.ModelClass.Caption;
            }
            else
            {
                return importingModel.ModelClass.ShortName;
            }
        }
        return ((ModelNode)importingModel).Id;
    }

    public static Type Get_ModelClassType(IModelImporting importingModel)
    {
        return importingModel.ModelClass?.TypeInfo.Type;
    }
}
