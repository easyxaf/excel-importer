﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.EF;
using System.Collections.ObjectModel;

namespace EasyXaf.Sample.EFCore.Module.BusinessObjects;

[DefaultClassOptions]
[DisplayProperty(nameof(Name))]
public class Department : BaseObject
{
    public virtual string Code { get; set; }

    public virtual string Name { get; set; }

    public virtual IList<ApplicationUser> Users { get; set; } = new ObservableCollection<ApplicationUser>();
}
