﻿using DevExpress.Persistent.BaseImpl.EF;
using EasyXaf.ExcelImporters;
using System.ComponentModel;

namespace EasyXaf.Sample.EFCore.Module.BusinessObjects;

[DisplayName("Excel导入")]
public class ExcelImporterTempFile : BaseObject, IExcelImporterTempFile
{
    [DisplayName("Excel文件")]
    public virtual FileData ExcelFile { get; set; }

    bool IExcelImporterTempFile.IsEmpty => ExcelFile == null || ExcelFile.IsEmpty;

    string IExcelImporterTempFile.FileName
    {
        get => ExcelFile.FileName;
        set => ExcelFile.FileName = value;
    }

    byte[] IExcelImporterTempFile.FileContent
    {
        get => ExcelFile.Content;
        set => ExcelFile.Content = value;
    }

    void IExcelImporterTempFile.InitFile(string fileName, byte[] fileContent)
    {
        ExcelFile = ObjectSpace.CreateObject<FileData>();
        ExcelFile.FileName = fileName;
        ExcelFile.Content = fileContent;
    }

    void IExcelImporterTempFile.SaveToStream(Stream stream)
    {
        ExcelFile.SaveToStream(stream);
    }
}
