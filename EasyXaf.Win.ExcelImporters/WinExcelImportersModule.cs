﻿using DevExpress.ExpressApp;
using EasyXaf.ExcelImporters;

namespace EasyXaf.Win.ExcelImporters;

public class WinExcelImportersModule : ModuleBase
{
    public WinExcelImportersModule()
    {
        RequiredModuleTypes.Add(typeof(ExcelImportersModule));
    }
}