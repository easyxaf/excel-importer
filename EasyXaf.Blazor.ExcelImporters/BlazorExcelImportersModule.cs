﻿using DevExpress.ExpressApp;
using EasyXaf.ExcelImporters;

namespace EasyXaf.Blazor.ExcelImporters;

public class BlazorExcelImportersModule : ModuleBase
{
    public BlazorExcelImportersModule()
    {
        RequiredModuleTypes.Add(typeof(ExcelImportersModule));
    }
}