﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace EasyXaf.Sample.Module.BusinessObjects;

[DefaultClassOptions]
[DisplayProperty(nameof(Name))]
public class Department : BaseObject
{
    public string Code
    {
        get => GetPropertyValue<string>(nameof(Code));
        set => SetPropertyValue(nameof(Code), value);
    }

    public string Name
    {
        get => GetPropertyValue<string>(nameof(Name));
        set => SetPropertyValue(nameof(Name), value);
    }

    [Association]
    public XPCollection<ApplicationUser> Users
    {
        get => GetCollection<ApplicationUser>(nameof(Users));
    }

    public Department(Session session)
        : base(session)
    {
    }
}
