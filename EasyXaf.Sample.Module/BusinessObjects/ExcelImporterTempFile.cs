﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using EasyXaf.ExcelImporters;

namespace EasyXaf.Sample.Module.BusinessObjects;

[System.ComponentModel.DisplayName("Excel导入")]
public class ExcelImporterTempFile : BaseObject, IExcelImporterTempFile
{
    [System.ComponentModel.DisplayName("Excel文件")]
    public FileData ExcelFile
    {
        get => GetPropertyValue<FileData>(nameof(ExcelFile));
        set => SetPropertyValue(nameof(ExcelFile), value);
    }

    bool IExcelImporterTempFile.IsEmpty => ExcelFile == null || ExcelFile.IsEmpty;

    string IExcelImporterTempFile.FileName
    {
        get => ExcelFile.FileName;
        set => ExcelFile.FileName = value;
    }

    byte[] IExcelImporterTempFile.FileContent
    {
        get => ExcelFile.Content;
        set => ExcelFile.Content = value;
    }

    public ExcelImporterTempFile(Session session)
        : base(session)
    {
    }

    void IExcelImporterTempFile.InitFile(string fileName, byte[] fileContent)
    {
        ExcelFile = new FileData(Session)
        {
            FileName = fileName,
            Content = fileContent
        };
    }

    void IExcelImporterTempFile.SaveToStream(Stream stream)
    {
        ExcelFile.SaveToStream(stream);
    }
}
