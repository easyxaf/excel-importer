﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.Sample.Module.BusinessObjects;

public enum Sex
{
    [XafDisplayName("男")]
    Male,

    [XafDisplayName("女")]
    Female
}
